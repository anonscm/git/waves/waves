RAVEN services - Bioinformatic services web application 
=======================================================

Authors
-------
* Marc Chakiachvili (LIRMM - UMR 5506 CNRS / UM - France)
* Floréal Cabanettes (LIRMM - UMR 5506 CNRS / UM  - France)
* Vincent Lefort (LIRMM - UMR 5506 CNRS / UM - France)
* Anne-Muriel Arigon Chiffoleau (LIRMM - UMR 5506 CNRS / UM - France)
* Vincent Berry (LIRMM - UMR 5506 CNRS / UM - France)

Features
--------
* Create and manage bioinformatic tools execution over such platform as
Galaxy server, DRMAA compliant cluster, Direct script execution,
API calls to other services...
* Presents these tools in a frontend based on Bootstrap3 and some 
AngularJS
* Follow and manage remote RESTful API access to your platform.

Documentation
-------------
More Documentation can be found on :
http://waves-bioinformatics.readthedocs.org/en/latest/

Project home page:
https://sourcesup.renater.fr/projects/waves/


License
-------
GPL licensed.
