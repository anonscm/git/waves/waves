/**
 * Created by marc on 10/05/16.
 */
tinymce.init({
    selector: '#id_description',
    resize: true,
    min_width: 350,
    width:650,
    themes: "inlite",
});