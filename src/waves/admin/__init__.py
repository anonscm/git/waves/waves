from .jobs import JobAdmin, JobHistoryInline, JobInputInline, JobOutputInline
from .runners import RunnerAdmin, RunnerParamInline
from .services import ServiceMetaInline, RelatedInputInline, ServiceAdmin, \
    ServiceCategoryAdmin, ServiceExitCodeInline
from .profiles import NewUserAdmin


