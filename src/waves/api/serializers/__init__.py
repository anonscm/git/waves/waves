from categories import CategorySerializer
from jobs import JobHistorySerializer, JobInputSerializer, JobSerializer, JobOutputSerializer
from waves.api.serializers.services import ServiceJobSerializer
from services import InputSerializer, MetaSerializer, OutputSerializer, \
    ServiceSerializer

