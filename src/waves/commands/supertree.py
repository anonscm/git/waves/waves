from __future__ import unicode_literals
from command import BaseCommand


class PhySIC_IST(BaseCommand):
    """
    Physic_IST algorithm decorator

    """
    pass


class MRP(BaseCommand):
    """
    MRP algorithm decorator
    """
    pass


class FastME(BaseCommand):
    pass
