from __future__ import unicode_literals
from .command import BaseCommand


class FastME(BaseCommand):
    """
    Fast Me Command decorator
    """
    pass
