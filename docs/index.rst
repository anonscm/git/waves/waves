#####
WAVES
#####


Introduction
=================================

Waves is intended to :
-  Easily present/manage/publish bioinformatic tools over such platform as
   Galaxy server, DRMAA compliant cluster, Direct script execution, API
   calls to other services...
-  Easily integrate these tools in a frontend based on Bootstrap3, use the `cms plugin` to include your job submission
   forms into your favorite CMS (Django-CMS or Mezzanine or via REST Call inside any other CMS)
-  Enable a RESTApi to provide your service to other platforms, follow, manage remote RESTful API accesses.

Installation
============

.. toctree::
   :maxdepth: 2

   :ref:`install`


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


